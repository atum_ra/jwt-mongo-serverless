const connectToDatabase = require('../common/db');
const User = require('../models/User');
const Article = require('../models/Article');
const { validateToken } = require('../auth/authorizer');
const debug = require('debug')('endpoints/createArticle');

module.exports.createArticle = (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  debug(event.queryStringParameters)
 
  return connectToDatabase()
    .then(() => {
       return validateToken(event.headers &&
        (event.headers.authorization || event.headers.Authorization))
        .then(decoded => User.model.findById(decoded.id).exec())
        .catch(err => {
          debug(err);
          throw err;
        })
    })
    .then( user => {
      debug('event.body', JSON.parse(event.body));
      const json = JSON.parse(event.body);
    //   let art = new Article({
    //     title: json.title || '',
    //     description: json.description || '',
    //     body: json.body || '',
    //     comments: [],
    //     author: user.id
    //   } );
    //  debug("toJSONFor", art.toJSONFor(user))
      return Article.create(
        {
          title: json.title || '',
          description: json.description || '',
          body: json.body || '',
          comments: [],
          author: user.id
        } 
      ) 
    })
    .then(currentArticle => {
      debug('article', currentArticle.toJSONFor());
      return {
        statusCode: 200,
        body: JSON.stringify({response:currentArticle.toJSON()})
      }
    })
    .catch(err => {
      debug('err', err);
      return {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: JSON.stringify({response: "error"})
      }
    });

};