const connectToDatabase = require('../common/db');
const User = require('../models/User');
const Article = require('../models/Article');
const debug = require('debug')('endpoints/listArticles');
const createError = require('http-errors');
const enablehttpErrorHandler = require('../common/middi');

const handler = (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  debug(event.queryStringParameters)
  const query = {};
  let author = '';
  let limit = 5;
  let offset = 0;
  if (event.queryStringParameters) {
    if (event.queryStringParameters.limit) limit = event.queryStringParameters.limit;
    if (event.queryStringParameters.offset) offset = event.queryStringParameters.offset;
    if (event.queryStringParameters.author )author = event.queryStringParameters.author;
  }

  return connectToDatabase()
    .then(() => {
      if (author) return User.model.findOne({ username })
        .exec();
      return null;
    })
    .then(author => {
      debug('author', author);
      if (author) query.author = author._id;
      // return Promise.all([
      return Article.find(query)
        .limit(Number(limit))
        .skip(Number(offset))
        .sort({ createdAt: 'desc' })
        .populate('author')
        .populate('comments')
        .exec();
    })
    .then(articles => {
      return {
        statusCode: 200,
        body:
          JSON.stringify({
            response: articles && articles[0] ?
              articles.map(article => {
                const { _id, __v, updatedAt, comments, ...rest } = article.toJSON();
                const username = rest.author ? rest.author.username : 'John Doe';
                let commentJSON = comments && comments[0] ? comments.map(comment => {
                  return {
                    body: comment.body,
                    createdAt: comment.createdAt
                  }
                }) : [];
                return Object.assign(rest, { author: username, comments: commentJSON });
              }) : []
          })
      }
    })
    .catch(err => {
      debug('catch error', err);
      throw createError(
        err.statusCode || 500,
        JSON.stringify({response: "error"}));
    });
};

module.exports.listArticles = enablehttpErrorHandler(handler);