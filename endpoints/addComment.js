const connectToDatabase = require('../common/db');
const User = require('../models/User');
const Article = require('../models/Article');
const Comment = require('../models/Comment');
const { validateToken } = require('../auth/authorizer');
const createError = require('http-errors');
const debug = require('debug')('endpoints/addComment');

module.exports.addComment = (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  const json = JSON.parse(event.body);

  return connectToDatabase()
    .then(() => {
      return validateToken(event.headers &&
        (event.headers.authorization || event.headers.Authorization))
        .then(decoded => User.model.findById(decoded.id).exec())
        .catch(err => {
          debug(err);
          throw err;
        })
    })
    .then((user) => {
      return Promise.all([
        Article.find({ title: event.pathParameters.title }).exec(),
        Promise.resolve(user)
      ]);
    })
    .then(([article, user]) => {
      if ( !article ) throw createError(404);
      if ( Array.isArray(article) && ( article.length>1 || article.length == 0 )) 
        throw createError(406);
      const comment = new Comment ({
        body: json.title || 'blank',
        author: user.id,
        article: article[0].id
      })
      return Promise.all([
        comment.save(comment),
        Promise.resolve(article[0]) 
      ])
    })
    .then(([comment, article]) => {
      debug("comment, article", comment, article)
      article.comments.push(comment);
      return Promise.all([
        article.save(),
        Promise.resolve(comment)
      ])
    })
    .then(([article, comment]) => {
      return {
        statusCode: 200,
        body: JSON.stringify({response:comment.toJSON()})
      }
    })
    .catch(err => {
      debug('err', err);
      return {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: JSON.stringify({response: "error"})
      }
    });

};