const connectToDatabase = require('../common/db');
const User = require('../models/User');
const qs = require('qs');
const {signToken} = require('./authorizer')
const debug = require('debug')('auth/signup')

module.exports.signup = (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return connectToDatabase()
    .then(() => {
      return register(qs.parse(event.body))
    })
    .then(session => {
      return {
        statusCode: 200,
        body: JSON.stringify(session)
      }
    })
    .catch(err => {
      return {
        statusCode: err.statusCode || 500,
        headers: { 'Content-Type': 'text/plain' },
        body: err.message
      }
    });
};

function register(eventBody) {
  return checkIfInputIsValid(eventBody) // validate input
    .then(() =>
      User.model.findOne({ email: eventBody.email }) // check if user exists
    )
    .then(currentUser =>
      currentUser
        ? Promise.reject(new Error('User with that email exists.'))
        : User.setPassword(eventBody.password) // hash the pass
    )
    .then(result => {
      const { salt, hash } = result;
      debug('register intodb', eventBody);
      // create the new user with Document instantiation
      return User.model.create({ username: eventBody.username, email: eventBody.email, hash, salt })
    })
    .then(currentUser => {
      return { auth: true, token: signToken(currentUser.id), userId: currentUser.id }; // sign the token and send it back
    })
    .catch(err => {
      console.error(err);
      throw err;
    })
}

  function checkIfInputIsValid(eventBody) {
    if (
      !(eventBody.password &&
        eventBody.password.length >= 7)
    ) {
      return Promise.reject(new Error('Password error. Password needs to be longer than 8 characters.'));
    }
    return Promise.resolve();
  }