const jwt = require('jsonwebtoken');
const debug = require('debug')('auth/authorizer');

const auth = (event, context, callback) => {

  // check header or url parameters or post parameters for token
  // into "Authorization" header
  const token = event.authorizationToken;
  return validateToken(token)
    .then(decoded => {
      return callback(null, generatePolicy(decoded.id, 'Allow', event.methodArn));
    })
    .catch(err => {
      debug(err);
    });
};

const validateToken = (token) => {
  if (!token) {
    debug('Unauthorized');
    return Promise.reject('Unauthorized');
  } 

  try {
    return Promise.resolve(jwt.verify(token, process.env.JWT_SECRET));
  } catch (err) {
    debug('Unauthorized, wrong token')
    return Promise.reject('Unauthorized');
  }
}

const signToken = (id) => {
  let k;
  try {
    k = jwt.sign({id}, process.env.JWT_SECRET, {
      expiresIn: 86400*3 // expires in 24*3 hours
    });    
  } catch (error) {
    return Promise.reject('signToken error');
  }
  return k;
}

const generatePolicy = (principalId, effect, resource) => {

  const authResponse = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument = {};
    policyDocument.Version = '2012-10-17';
    policyDocument.Statement = [];
    const statementOne = {};
    statementOne.Action = 'execute-api:Invoke';
    statementOne.Effect = effect;
    statementOne.Resource = resource;
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  return authResponse;
}

module.exports = {
  auth,
  signToken,
  validateToken
};