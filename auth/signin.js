const connectToDatabase = require('../common/db');
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const {signToken} = require('./authorizer')
const debug = require('debug')('auth/signin')


module.exports.signin = (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return connectToDatabase()
    .then(() =>
      login(JSON.parse(event.body))
    )
    .then(session => ({
      statusCode: 200,
      body: JSON.stringify(session)
    }))
    .catch(err => ({
      statusCode: err.statusCode || 500,
      headers: { 'Content-Type': 'text/plain' },
      body: JSON.stringify({ message: "error" })
    }));
};

function login(eventBody) {
  return User.model.findOne({ email: eventBody.email })
    .then(currentUser =>
      !currentUser
        ? Promise.reject(new Error('User with that email does not exits.'))
        : comparePassword(eventBody.password, currentUser1)
    )
    .then(token => ({ auth: true, token: token }))
    .catch(err => {
      console.error(err);
      throw err;
    });
}

function comparePassword(eventPassword, currentUser) {
  return Promise.resolve(User.validPassword(eventPassword, currentUser))
    .then(passwordIsValid =>
      !passwordIsValid
        ? Promise.reject(new Error('The credentials do not match.'))
        : signToken(currentUser._id)
    )
}
