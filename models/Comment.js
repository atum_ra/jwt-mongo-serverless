const mangoose = require('mongoose');
const User = require('./User');

const CommentSchema = new mangoose.Schema({
  body: {
    type: String,
    required: [true, "can't be blank"]
  },
  author: { 
    type: 'ObjectId',
    ref: 'User',
    required: [true, "can't be blank"]
   },
  article: { 
    type: 'ObjectId',
    ref: 'Article',
    required: [true, "can't be blank"]
   }
},
  { timestamps: true });

class Comment {
  toJSONFor = () => {
    return {
      body: this.body,
      createdAt: this.createdAt
    }
  }
}

CommentSchema.loadClass(Comment);
module.exports = mangoose.model('Comment', CommentSchema);