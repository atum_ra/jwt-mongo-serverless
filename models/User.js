const mangoose = require('mongoose');  
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const debug = require('debug')('model/User')

const UserSchema = new mangoose.Schema(
  {
      username: {
          type: String,
          lowercase: true,
          unique: true,
          required: [true, "can't be blank"],
          match: [/^[a-zA-Z0-9]+$/, 'is invalid']
      },
      email: {
          type: String,
          lowercase: true,
          unique: true,
          required: [true, "can't be blank"],
          match: [/\S+@\S+\.\S+/, 'is invalid']
      },
      bio: String,
      image: String,
      hash: String,
      salt: String,
      scope: [String]
  },
  { timestamps: true }
);

const user = {};

user.setPassword = (password) => {
    let salt = crypto.randomBytes(16).toString('hex');
    let hash = crypto
        .pbkdf2Sync(password, salt, 10000, 512, 'sha512')
        .toString('hex');
    return {salt, hash};
};

user.validPassword = (password, currentUser) => {
  let salt = currentUser.salt || '';
  let dbHash = currentUser.hash || '';
  const hash = crypto
  .pbkdf2Sync(password, salt, 10000, 512, 'sha512')
  .toString('hex');
  return hash === dbHash;
};

user.toAuthJSON = (currentUser) => {
    return {
        username: currentUser.username || 'empty',
        email: currentUser.email || 'empty',
        token: user.generateJWT(currentUser),
        bio: currentUser.bio || '' ,
        image: currentUser.image || ''
    };
};

user.toProfileJSONFor = (currentUser) => {
    return {
        username: currentUser.username || 'empty',
        bio: currentUser.bio || '' ,
        image:
            this.image ||
            'https://randomuser.me/api/portraits/lego/2.jpg '
    };
};
user.model = mangoose.model('User', UserSchema);
// Each document is an instance of its Model.
module.exports = user;