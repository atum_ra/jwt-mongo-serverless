const mangoose = require('mongoose'); 
const User = require('./User');
const Comment = require('./Comment');

const ArticleSchema = new mangoose.Schema(
    {
        title: String,
        description: String,
        body: String,
        comments: [{ type: 'ObjectId', ref: 'Comment' }],
        author: { type: 'ObjectId', ref: 'User' }
    },
    { timestamps: true }
);

// Do not see methods into class
class Article {
  toJSONFor (userToShowProfile) {
    return {
      title: this.title || '',
      description: this.description || '',
      body: this.body || '',
      author: this.user ? User.toProfileJSONFor(userToShowProfile) : ''
    }
  }
}

ArticleSchema.loadClass(Article);
module.exports = mangoose.model('Article', ArticleSchema);