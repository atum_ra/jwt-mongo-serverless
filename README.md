## Servereless
This folder presents some templates for serverless approach in cusual dvpm. Some of these I reported at Barcelona Cloud development meetup in 2019.

>The most common ways to implement authentication and authorization are storing user sessions or writing user information inside JSON Web Tokens. 

Typically, you would store session data in either Redis or Memcached.  But JWT, you send every time, has a key advantage; it makes it easy to store additional user information directly in the token, not just the access credentials. On every request, the user will send the whole token to the endpoint. If you store their username or access scopes in the JWT token, it will be very easy to access that information in every HTTP request you receive.

### So when and where should you check the user’s credentials inside your app?

One solution would be to check the JWT or session content on every call to any of your functions. This gives you the most control over the authentication flow, but it is complicated to implement; you have to do everything yourself. It’s also not optimal from the database access standpoint.

Another solution that improves on some of these issues is using Custom Authorizers supported by API Gateway.

### Notes

+ Here I designed OOP approach with classes for node.js v12. There is also User module with old approach, in legacy code it could be mixed. 
+ logging based on Winston logger lib (used decorator pattern to tune logs for fast log procession during debug mode), 
```
npm start offline
```
+ Serverless framework, 
+ Newton for integration tests and Jasmine for unit tests.
+ As mentioned above I used 2 approaches for control access: the first with Lambda authoriser custom function and the second is inside the handler, based on JWT token and user data.
+ There is also authorizer-with-scope module, it is custom lambda function with authorization (level of access) opportunity for separate right to access

For tests propose it is easy to use mongo docker container
```
docker run --rm -d -p 27017:27017/tcp mongo:latest
```