const mongoose = require('mongoose');
const debug = require('debug')('./db')
let isConnected;

module.exports = connectToDatabase = () => {
  if (isConnected) {
    debug('=> using existing database connection');
    return Promise.resolve();
  }

  debug('=> using new database connection');
  return mongoose.connect(process.env.DB)
    .then(db => { 
      isConnected = db.connections[0].readyState;
    });
};