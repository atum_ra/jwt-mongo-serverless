const middy = require('@middy/core')
const httpErrorHandler = require('@middy/http-error-handler')

const enablehttpErrorHandler = (handler) => middy(handler)
  .use(httpErrorHandler())

module.exports = enablehttpErrorHandler;